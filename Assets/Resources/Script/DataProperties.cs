using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;

[CreateAssetMenu(fileName = "MyScriptableObject", menuName = "DataProperties")]
public class DataProperties : ScriptableObject
{
    public int intValue;
    public float floatValue;
    public string stringValue;
}
