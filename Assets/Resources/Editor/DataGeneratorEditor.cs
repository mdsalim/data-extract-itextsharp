using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;
using UnityEditor;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Path = System.IO.Path;
using System.Text.RegularExpressions;

[CustomEditor(typeof(DataGenerator))]
public class DataGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DataGenerator myObject = (DataGenerator)target;

        if (GUILayout.Button("ScriptableObject to XML"))
        {
            ScriptableObjectToXML(myObject.DataProperties);
        }
        if (GUILayout.Button("Created XML To New ScriptableObject"))
        {
            CreatedXmlToScriptableObject(Application.dataPath + "/FullChatdata.xml");
        }
        if(GUILayout.Button("ScriptableObject to Excel"))
        {
            ScriptableObjectToExcel(myObject.DataProperties);
        }
        if(GUILayout.Button("Excel to ScriptableObject"))
        {
            CreatedexcelToScriptableObject(Application.dataPath + "/exporter.csv");
        }
        if(GUILayout.Button("PDF To ScriptableObject"))
        {
            PDFToScriptableObject(Application.dataPath + Path.Combine(Application.persistentDataPath, "/outputer.pdf"), "password");
        }
        
    }
    public void ScriptableObjectToXML(DataProperties dataProperties)
    {
        // Create an instance of the XmlSerializer
        XmlSerializer serializer = new XmlSerializer(typeof(DataProperties));

        // Create a StringWriter to hold the XML data
        StringWriter stringWriter = new StringWriter();

        // Serialize the data to XML
        serializer.Serialize(stringWriter, dataProperties);

        // Save the XML data to a file
        File.WriteAllText(Application.dataPath + "/FullChatdata.xml", stringWriter.ToString());

        Debug.Log("XML data imported successfully!");
    }

    public void CreatedXmlToScriptableObject(string xmlFilePath)
    {
        // Create an instance of the XmlSerializer
        XmlSerializer serializer = new XmlSerializer(typeof(DataProperties));

        // Open the XML file
        StreamReader reader = new StreamReader(xmlFilePath);

        // Deserialize the XML data into a new instance of DeserializedData
        DataProperties newData = (DataProperties)serializer.Deserialize(reader);

        // Close the file
        reader.Close();

        // Now you have the deserialized data in newData
        // You can use it as a new ScriptableObject instance or copy its values to an existing one
        // For example, you can create a new ScriptableObject and assign the deserialized data to it
        DataProperties newScriptableObject = ScriptableObject.CreateInstance<DataProperties>();

        newScriptableObject.intValue = newData.intValue;
        Debug.Log(newData.intValue);
        newScriptableObject.floatValue = newData.floatValue;
        Debug.Log(newData.floatValue);
        newScriptableObject.stringValue = newData.stringValue;
        Debug.Log(newData.stringValue);

        string assetPath = Path.Combine("Assets", Path.GetFileNameWithoutExtension(xmlFilePath) + ".asset");

        UnityEditor.AssetDatabase.CreateAsset(newData, assetPath);

        Debug.Log("ScriptableObject converted to XML and saved as ");
        ScriptableObjectToPDF(newData);
    }

    public void ScriptableObjectToPDF(DataProperties dataProperties)
    {
        // Create a new PDF document
        Document document = new Document();

        // Set the path to save the PDF file
        string outputPath = Path.Combine(Application.persistentDataPath, "/outputer.pdf");

        // Create a PdfWriter instance to write the document to a file
        PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(Application.dataPath + outputPath, FileMode.Create));

        // Open the document
        document.Open();

        // Create a new paragraph and add the ScriptableObject data to it
        Paragraph paragraph = new Paragraph();
        paragraph.Add("Int Value: " + dataProperties.intValue.ToString());
        paragraph.Add("\nFloat Value: " + dataProperties.floatValue.ToString());
        paragraph.Add("\nString Value: " + dataProperties.stringValue);

        // Add the paragraph to the document
        document.Add(paragraph);

        // Close the document
        document.Close();

        Debug.Log("PDF created at: " + outputPath);
    }

    public void PDFToScriptableObject(string filePath,string password)
    {
        // Create a new ScriptableObject instance
        DataProperties dataProperties = ScriptableObject.CreateInstance<DataProperties>();

        // Create a new PDF reader with the password
        PdfReader reader = new PdfReader(filePath, System.Text.Encoding.Default.GetBytes(password));

        // Extract the necessary data from the PDF
        for (int i = 1; i <= reader.NumberOfPages; i++)
        {
            string extractedText = PdfTextExtractor.GetTextFromPage(reader, i);

            //dataProperties.stringValue += extractedText;

            Regex intValueRegex = new Regex(@"Int Value: (\d+)");
            Match intValueMatch = intValueRegex.Match(extractedText);
            if (intValueMatch.Success)
            {
                if (int.TryParse(intValueMatch.Groups[1].Value, out int intValue))
                {
                    dataProperties.intValue = intValue;
                }
            }

            Regex floatValueRegex = new Regex(@"Float Value: ([\d.]+)");
            Match floatValueMatch = floatValueRegex.Match(extractedText);
            if (floatValueMatch.Success)
            {
                if (float.TryParse(floatValueMatch.Groups[1].Value, out float floatValue))
                {
                    dataProperties.floatValue = floatValue;
                }
            }

            Regex stringValueRegex = new Regex(@"String Value: (.+)");
            Match stringValueMatch = stringValueRegex.Match(extractedText);
            if (stringValueMatch.Success)
            {
                dataProperties.stringValue = stringValueMatch.Groups[1].Value;
            }
        }

        // Close the PDF reader
        reader.Close();

        // Create a unique asset name
        string assetName = "ScriptableObjectAsset";
        string assetPath = "Assets/" + assetName + ".asset";

        // Save the ScriptableObject asset
        AssetDatabase.CreateAsset(dataProperties, assetPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        // Log the asset path
        Debug.Log("ScriptableObject asset created at: " + assetPath);
    }

    public void ScriptableObjectToExcel(DataProperties dataProperties)
    {
        
        // Create a new StringBuilder to store the CSV data
        StringBuilder sb = new StringBuilder();

        // Append the header row
        sb.AppendLine("IntValue, FloatValue, StringValue");

        // Iterate over the data and append each row
        sb.AppendLine(dataProperties.intValue + ", " + dataProperties.floatValue + ", " + dataProperties.stringValue);

        // Define the file path and name
        string filePath = Application.dataPath + "/exporter.csv";

        // Write the CSV data to the file
        File.WriteAllText(filePath, sb.ToString());

        // Open the CSV file in Excel (if Excel is installed)
        System.Diagnostics.Process.Start(filePath);

        Debug.Log("File path :" + filePath);
    }
    
    public DataProperties CreatedexcelToScriptableObject(string filePath)
    {
        // Read the CSV file
        string[] csvLines = File.ReadAllLines(filePath);

        // Create a new instance of the ScriptableObject
        DataProperties dataObject = ScriptableObject.CreateInstance<DataProperties>();

        // Iterate over the CSV lines (skipping the header)
        for (int i = 1; i < csvLines.Length; i++)
        {
            string[] csvValues = csvLines[i].Split(',');

            // Parse and assign the values
            dataObject.intValue = int.Parse(csvValues[0]);
            dataObject.floatValue = float.Parse(csvValues[1]);
            dataObject.stringValue = csvValues[2];
        }

        string assetPath = Path.Combine("Assets", Path.GetFileNameWithoutExtension(filePath) + ".asset");

        UnityEditor.AssetDatabase.CreateAsset(dataObject, assetPath);

        Debug.Log("CSV path :" + filePath);
        return dataObject;
    }
}
